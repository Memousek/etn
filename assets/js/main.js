
document.querySelector(".current__year").innerHTML = new Date().getFullYear();

$('.section__icon, .content__close').click(function () {
    $('.contact__content').toggleClass('visible')
});


$(document).ready(function () {

    $(".hamburger__button").on("click", function () {
        $(this).parent().toggleClass("hamburger--open");
        $("body").toggleClass("mobile-nav-open");

        if ($("#mobile-nav").hasClass("open")) {
            $("mobile-nav").removeClass("open");
            $("#mobile-nav").toggleClass("closed");
        } else {
            $("#mobile-nav").toggleClass("open");
        }
    })

    $(".hamburger_button--open").on("click", function () {
        $("body").removeClass("mobile-nav-open")
    })
    //collapse mobile nav polozek
    const mobileNav = $("#mobile-nav > ul");

    let activeItem = $("#mobile-nav .nav__item.active");

    if (mobileNav.length > 0) {

        let NavItems = mobileNav.children("li");

        let NavToggler = $("#mobile-nav > ul li .toggle-down");


        collapseMenu(NavItems);

        $(document).on("click", ".toggle-down", function () {
            $(this).parent().toggleClass("open");

            $(this).siblings(".nav").slideToggle(300);
        });

        if (activeItem.length > 0) {
            let activeItemParents = $(activeItem).parentsUntil("#mobile-nav");

            activeItemParents.each(function () {
                $(this).toggleClass("open");
                $(this).slideDown();
            })

        }



        function openActive(ïtem) {
            let stepUp = $(item).parent();

        }
        // console.log($(NavToggler));

        // console.log($submenu);


        function collapseMenu(element) {

            element.each(function () {

                let submenu = $(this).children("ul");


                if (submenu.length > 0) {
                    $(this).toggleClass("collapsed");
                    $(this).append("<span class='toggle-down'></span>");

                    $(submenu).hide();


                    // $(this).hide();

                    // let submenu2 = $(submenu).find("ul");
                    //
                    // submenu2.each(function () {
                    //     $(this).parent().toggleClass("collapsed");
                    //     $(this).parent().append("<span class='toggle-down'></span>");
                    //
                    //     // console.log(($($(this).parent()).height()));
                    //     $(this).parent().hide();
                    // })

                    collapseMenu(submenu.children(("li")));

                }

                // console.log($(object).children("ul"));
            });


        }

    }
});


const hours = [
    {
        time: "8:00",
        occupancy: [35, 50]
    },
    {
        time: "9:00",
        occupancy: [50, 50]
    },
    {
        time: "10:00",
        occupancy: [30, 25]
    },
    {
        time: "11:00",
        occupancy: [50, 100]
    },
    {
        time: "12:00",
        occupancy: [95, 80]
    },
    {
        time: "13:00",
        occupancy: [50, 50]
    },
    {
        time: "14:00",
        occupancy: [50, 25]
    },
    {
        time: "15:00",
        occupancy: [30, 50]
    },
    {
        time: "16:00",
        occupancy: [35, 80]
    },
    {
        time: "17:00",
        occupancy: [60, 75]
    },
    {
        time: "18:00",
        occupancy: [80, 80]
    },
    {
        time: "19:00",
        occupancy: [50, 50]
    },
    {
        time: "20:00",
        occupancy: [70, 50]
    },
    {
        time: "21:00",
        occupancy: [70, 50]
    },
    {
        time: "22:00",
        occupancy: [30, 10]
    }
];

let graph = $('#graph');

hours.forEach(function (item) {
    let itemEl = document.createElement("div");
    let occupancyColorClass = null;
    let occupancyColorClass2 = null;
    itemEl.classList.add('graph-item');

    if (item.occupancy[0] <= 25) {
        occupancyColorClass = "low";
    } else if (item.occupancy[0] <= 50) {
        occupancyColorClass = "medium";
    } else if (item.occupancy[0] <= 75) {
        occupancyColorClass = "high";
    } else {
        occupancyColorClass = " max";
    }

    if (item.occupancy[1] <= 25) {
        occupancyColorClass2 = "low";
    } else if (item.occupancy[1] <= 50) {
        occupancyColorClass2 = "medium";
    } else if (item.occupancy[1] <= 75) {
        occupancyColorClass2 = "high";
    } else {
        occupancyColorClass2 = " max";
    }
    let itemContent = `
    <span class="occupancy-time">
    ${item.time}
    </span>
    <div class="occupancy-wrapper">
      <span class="occupancy ${occupancyColorClass}" style="width: ${item.occupancy[0]}%"></span>
      <span class="occupancy ${occupancyColorClass2}" style="width: ${item.occupancy[1]}%"></span>
    </div>
  `;
    itemEl.innerHTML = itemContent;
    graph.append(itemEl);
})

