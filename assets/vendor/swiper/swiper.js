import Swiper from "./swiper-bundle.min.js";

var swiper = new Swiper(".hero__swiper", {
    navigation: {
        nextEl: ".swiper-button-next",
        prevEl: ".swiper-button-prev",
    },
    //loop: true,
    autoplay: {
        delay: 19999000,
    }
});